name = "python"

version = "3.10.8"

variants = [
    ["platform-windows", "arch-AMD64"],
]

build_command = ""

with scope("config") as c:
    c.release_packages_path = r"D:\rez\pkgs\ext"


def commands():
    env.PATH.append(this._python_dir)


@early()
def _python_dir():
    import os

    python_dir = os.path.join(os.getenv("PROGRAMFILES"), "Python", str(this.version))
    return python_dir
